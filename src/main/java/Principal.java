package main.java;

import java.util.Scanner;
import java.util.Random;

public class Principal{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int x=0, y=0;
        boolean [][] barcos = new boolean[10][10];
        cargarMatriz(barcos);
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 15; i++) {
            System.out.println("Introduce unas coordenadas (X, Y) para disparar");
            x=sc.nextInt();
            y=sc.nextInt();
            juego(barcos, x, y);
        }
        System.out.println("Partida finalizada");
    }
    
    public static void cargarMatriz(boolean[][] matriz){
        Scanner sc=new Scanner(System.in);
        int i=0;
        Random n = new Random();
        Random n2 = new Random();
        for(int f=0; f<matriz.length;f++){
            for (int c=0; c < matriz[f].length; c++) {
                while(i<10){
                    matriz[n.nextInt(10)][n2.nextInt(10)]=true;
                    i++;
                }
            }
        }
    }
    
    public static void juego(boolean[][] matriz, int fila, int col){
        if (matriz[fila][col]==true) {
            System.out.println("hundido");
            matriz[fila][col]=false;
        }else{
            System.out.println("agua");
        }
    }
    
    
    /* Código para pruebas
     * 
    public static void pruebas(){
    Scanner sc=new Scanner(System.in);
    int a;
   int c =sc.nextInt();
   System.out.println(c);
   try{
       a=10/c;
       System.out.println("valor de a = " + a);
   }catch (ArithmeticException e){
       System.out.println("Primera excepción: división por cero\n" + e.getClass());
   }catch (InputMismatchException e){
       System.out.println("Segunda excepción: tipo de dato incorrecto\n" + e.getClass());
   }catch (Exception e){
       System.out.println("Excepción" + e.getClass().getSimpleName());
   }
}
}

*/
}
